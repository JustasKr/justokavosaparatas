﻿namespace KavosAparatas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.buttonJuoda = new System.Windows.Forms.Button();
            this.buttonBalta = new System.Windows.Forms.Button();
            this.buttonLatte = new System.Windows.Forms.Button();
            this.buttonMocha = new System.Windows.Forms.Button();
            this.buttonCappuccino = new System.Windows.Forms.Button();
            this.buttonPapildziau = new System.Windows.Forms.Button();
            this.buttonmas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ekranas.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Ekranas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Ekranas.Location = new System.Drawing.Point(5, 12);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(496, 113);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ekranas.TextChanged += new System.EventHandler(this.Ekranas_TextChanged);
            // 
            // buttonJuoda
            // 
            this.buttonJuoda.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonJuoda.Location = new System.Drawing.Point(28, 131);
            this.buttonJuoda.Name = "buttonJuoda";
            this.buttonJuoda.Size = new System.Drawing.Size(172, 31);
            this.buttonJuoda.TabIndex = 1;
            this.buttonJuoda.Text = "Juoda";
            this.buttonJuoda.UseVisualStyleBackColor = true;
            this.buttonJuoda.Click += new System.EventHandler(this.buttonJuoda_Click);
            // 
            // buttonBalta
            // 
            this.buttonBalta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonBalta.Location = new System.Drawing.Point(28, 168);
            this.buttonBalta.Name = "buttonBalta";
            this.buttonBalta.Size = new System.Drawing.Size(172, 31);
            this.buttonBalta.TabIndex = 2;
            this.buttonBalta.Text = "Balta";
            this.buttonBalta.UseVisualStyleBackColor = true;
            this.buttonBalta.Click += new System.EventHandler(this.buttonBalta_Click);
            // 
            // buttonLatte
            // 
            this.buttonLatte.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonLatte.Location = new System.Drawing.Point(28, 205);
            this.buttonLatte.Name = "buttonLatte";
            this.buttonLatte.Size = new System.Drawing.Size(172, 31);
            this.buttonLatte.TabIndex = 3;
            this.buttonLatte.Text = "Latte";
            this.buttonLatte.UseVisualStyleBackColor = true;
            this.buttonLatte.Click += new System.EventHandler(this.buttonLatte_Click);
            // 
            // buttonMocha
            // 
            this.buttonMocha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonMocha.Location = new System.Drawing.Point(28, 242);
            this.buttonMocha.Name = "buttonMocha";
            this.buttonMocha.Size = new System.Drawing.Size(172, 31);
            this.buttonMocha.TabIndex = 4;
            this.buttonMocha.Text = "Mocha";
            this.buttonMocha.UseVisualStyleBackColor = true;
            this.buttonMocha.Click += new System.EventHandler(this.buttonMocha_Click);
            // 
            // buttonCappuccino
            // 
            this.buttonCappuccino.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonCappuccino.Location = new System.Drawing.Point(28, 279);
            this.buttonCappuccino.Name = "buttonCappuccino";
            this.buttonCappuccino.Size = new System.Drawing.Size(172, 31);
            this.buttonCappuccino.TabIndex = 5;
            this.buttonCappuccino.Text = "Cappuccino";
            this.buttonCappuccino.UseVisualStyleBackColor = true;
            this.buttonCappuccino.Click += new System.EventHandler(this.buttonCappuccino_Click);
            // 
            // buttonPapildziau
            // 
            this.buttonPapildziau.Location = new System.Drawing.Point(432, 264);
            this.buttonPapildziau.Name = "buttonPapildziau";
            this.buttonPapildziau.Size = new System.Drawing.Size(69, 46);
            this.buttonPapildziau.TabIndex = 6;
            this.buttonPapildziau.Text = "Papildžiau";
            this.buttonPapildziau.UseVisualStyleBackColor = true;
            this.buttonPapildziau.Click += new System.EventHandler(this.buttonPapildziau_Click);
            // 
            // buttonmas
            // 
            this.buttonmas.Location = new System.Drawing.Point(426, 131);
            this.buttonmas.Name = "buttonmas";
            this.buttonmas.Size = new System.Drawing.Size(75, 23);
            this.buttonmas.TabIndex = 7;
            this.buttonmas.Text = "masyvas";
            this.buttonmas.UseVisualStyleBackColor = true;
            this.buttonmas.Click += new System.EventHandler(this.buttonmas_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 331);
            this.Controls.Add(this.buttonmas);
            this.Controls.Add(this.buttonPapildziau);
            this.Controls.Add(this.buttonCappuccino);
            this.Controls.Add(this.buttonMocha);
            this.Controls.Add(this.buttonLatte);
            this.Controls.Add(this.buttonBalta);
            this.Controls.Add(this.buttonJuoda);
            this.Controls.Add(this.Ekranas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button buttonJuoda;
        private System.Windows.Forms.Button buttonBalta;
        private System.Windows.Forms.Button buttonLatte;
        private System.Windows.Forms.Button buttonMocha;
        private System.Windows.Forms.Button buttonCappuccino;
        private System.Windows.Forms.Button buttonPapildziau;
        private System.Windows.Forms.Button buttonmas;
    }
}

