﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace KavosAparatas
{
    public partial class Form1 : Form
    {

        int puodukuSk = 0;
                      
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonJuoda_Click(object sender, EventArgs e)
        {
            Atvaizduok("Juoda Kava ");

        }

        private void Ekranas_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonBalta_Click(object sender, EventArgs e)
        {
            Atvaizduok("Balta Kava ");
        }

        private void buttonLatte_Click(object sender, EventArgs e)
        {
            Atvaizduok("Latte ");
        }

        private void buttonMocha_Click(object sender, EventArgs e)
        {
            Atvaizduok("Mocha ");
        }

        private void buttonCappuccino_Click(object sender, EventArgs e)
        {
            Atvaizduok("Cappuccino ");
        }

        private void Atvaizduok(string kava)
        {

            if (puodukuSk >= 10)
            {
                Ekranas.Text = "Kava baigėsi, papildykite!";

                return;
            }

            for (int i = 0; i < 100; i++)
            {

                Ekranas.Text = i.ToString();
                Thread.Sleep(10);
                Ekranas.Refresh();
                                  
            }

            Ekranas.Text = kava + " Pagaminta!";

            puodukuSk++;

            {
                return;
            }
        }

        private void buttonPapildziau_Click(object sender, EventArgs e)
        {
            Password testDialog = new Password();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {

                if (testDialog.slapt.Text == "atrakink")
                {

                    Ekranas.Text = "";
                    puodukuSk = 0;

                    MessageBox.Show("Papildymas atliktas");
                }

                

            }
      


            
        }

        private void buttonmas_Click(object sender, EventArgs e)
        {

            int[] masyvas = new int[5];
            masyvas[0] = 2;
            masyvas[1] = 4;
            masyvas[2] = 6;
            masyvas[3] = 8;
            masyvas[4] = 10;

            Ekranas.Text = masyvas[3].ToString();

        }
    }
}
