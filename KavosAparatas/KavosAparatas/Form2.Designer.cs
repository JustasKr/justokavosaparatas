﻿namespace KavosAparatas
{
    partial class Password
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSlaptazodis = new System.Windows.Forms.Button();
            this.slapt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonSlaptazodis
            // 
            this.buttonSlaptazodis.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonSlaptazodis.Location = new System.Drawing.Point(60, 95);
            this.buttonSlaptazodis.Name = "buttonSlaptazodis";
            this.buttonSlaptazodis.Size = new System.Drawing.Size(142, 53);
            this.buttonSlaptazodis.TabIndex = 0;
            this.buttonSlaptazodis.Text = "OK";
            this.buttonSlaptazodis.UseVisualStyleBackColor = true;
            this.buttonSlaptazodis.Click += new System.EventHandler(this.buttonSlaptazodis_Click);
            // 
            // slapt
            // 
            this.slapt.Location = new System.Drawing.Point(27, 60);
            this.slapt.Name = "slapt";
            this.slapt.Size = new System.Drawing.Size(224, 20);
            this.slapt.TabIndex = 1;
            this.slapt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter Password:";
            // 
            // Password
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.slapt);
            this.Controls.Add(this.buttonSlaptazodis);
            this.Name = "Password";
            this.Text = "Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSlaptazodis;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox slapt;
    }
}